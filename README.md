# README #

* KISS.py :
    Contains the cryptographic layer

* lab2stack.py :
    Original RIP stack

* lab3stack.py :
    Connector factory, stacks KISS layer on top of our RIP layer

* KISS_RFC.txt : Rendering of the KISS RFC, source XML can be found in KISS_RFC.xml

Use lab3stack as a stack to run the two layers combined
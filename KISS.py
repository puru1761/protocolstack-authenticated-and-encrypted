from Crypto.PublicKey import RSA
from playground.crypto import X509Certificate
from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto.Cipher.PKCS1_OAEP import PKCS1OAEP_Cipher
import CertFactory

from playground.network.common.Protocol import StackingTransport, \
    StackingProtocolMixin, StackingFactoryMixin
from playground.network.common.Protocol import MessageStorage
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING, DEFAULT_VALUE
from twisted.internet.protocol import Protocol, Factory
import os

class KISSHandshakeMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "KISSHandshake"
    MESSAGE_VERSION = "1.0"
    BODY = [("key", STRING, DEFAULT_VALUE("")), ("IV", STRING, DEFAULT_VALUE(""))]

class KISSDataMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "KISSData"
    MESSAGE_VERSION = "1.0"
    BODY = [("data", STRING, DEFAULT_VALUE(""))]

class EncryptionHelper():

    def __init__(self):
        self.key = os.urandom(32)
        self.IV = os.urandom(16)

class DecryptionHelper():

    def __int__(self):
        self.peerKey = ""
        self.peerIV = ""

class KISSTransport(StackingTransport):

    def __init__(self, protocol, lowerTransport):
        self.protocol = protocol
        self.ctr = Counter.new(128, initial_value=int(self.protocol.en.IV.encode('hex'), 16))
        self.Encryptor = AES.new(self.protocol.en.key, counter=self.ctr, mode=AES.MODE_CTR)
        StackingTransport.__init__(self, lowerTransport)

    def write(self, data):
        msg = KISSDataMessage()
        msg.data = self.Encryptor.encrypt(data)
        self.lowerTransport().write(msg.__serialize__())

    def loseConnection(self):
        self.lowerTransport().loseConnection()



class KISSProtocol(Protocol, StackingProtocolMixin):

    def __init__(self):
        self.en=EncryptionHelper()
        self.dh=DecryptionHelper()
        self.storage = MessageStorage()

    def connectionMade(self):
        self.HigherTransport = KISSTransport(self, self.transport)
        self.sendHandshake()

    def dataReceived(self, data):
        self.storage.update(data)

        for msg in self.storage.iterateMessages():
            packet = msg

            if(packet.PLAYGROUND_IDENTIFIER=="KISSHandshake"):
                #print "Got Handshake!"
                self.MakeDecryptionHelper(packet.key, packet.IV)
                self.connectionMaker()
                #print self.en.peerKey.encode('hex'), self.en.peerIV.encode('hex')
            elif(packet.PLAYGROUND_IDENTIFIER=="KISSData"):
                d = self.Decryptor.decrypt(packet.data)
                self.higherProtocol().dataReceived(d)


    def connectionLost(self, reason):
        self.en = EncryptionHelper()
        self.dh = DecryptionHelper()
        self.higherProtocol().connectionLost(self.HigherTransport)

    def sendHandshake(self):
        Msg = KISSHandshakeMessage()
        Msg.key, Msg.IV = self.KeyEncryptor(self.en.key, self.en.IV)
        self.transport.write(Msg.__serialize__())

    def connectionMaker(self):
        self.makeHigherConnection(self.HigherTransport)

    def MakeDecryptionHelper(self, key, IV):
        self.dh.peerKey, self.dh.peerIV = self.KeyDecryptor(key, IV)
        self.ctr = Counter.new(128, initial_value=int(self.dh.peerIV.encode('hex'), 16))
        self.Decryptor = AES.new(self.dh.peerKey, counter=self.ctr, mode=AES.MODE_CTR)

    def KeyEncryptor(self, key, IV):
        cert = self.transport.getPeer().certificateChain[0]
        peerPublicKeyBlob = cert.getPublicKeyBlob()
        peerPublicKey = RSA.importKey(peerPublicKeyBlob)
        peerRsaEncrypter = PKCS1OAEP_Cipher(peerPublicKey, None, None, None)
        encKey = peerRsaEncrypter.encrypt(key)
        encIV = peerRsaEncrypter.encrypt(IV)

        return encKey,encIV

    def KeyDecryptor(self, key, IV):
        key1 = self.transport.getHost().privateKey
        peerRsaDecrypter = PKCS1OAEP_Cipher(key1, None, None, None)
        encKey = peerRsaDecrypter.decrypt(key)
        encIV = peerRsaDecrypter.decrypt(IV)

        return encKey,encIV



class KISSFactory(Factory, StackingFactoryMixin):
    protocol = KISSProtocol


ConnectFactory = KISSFactory()
ListenFactory = KISSFactory()
